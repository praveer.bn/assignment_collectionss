package week6.Assignment_collection;

import java.util.ArrayList;
import java.util.ListIterator;


public class ListIterrator {

    public static void main(String[] args) {
        ArrayList<Integer> arr=new ArrayList<>();
        arr.add(10);
        arr.add(9);
        arr.add(8);
        arr.add(7);
        arr.add(6);
        arr.add(5);
        arr.add(4);
        System.out.println(arr.get(3));
        System.out.println(arr.set(3,100));
        System.out.println(arr.set(5,100));
        System.out.println(arr.contains(100));
        System.out.println(arr.indexOf(100));
        arr.add(6,200);
        System.out.println(arr.lastIndexOf(100));
        System.out.println(arr.size());
        System.out.println(arr.isEmpty());
        System.out.println(arr.toArray());
        System.out.println(arr.subList(2,5));
        //for each
//        for (Integer index:arr
//             ) {
//            System.out.println(index);
//        }

        ListIterator itr=arr.listIterator(arr.size());
        while (itr.hasPrevious()){
            System.out.println(itr.previous());
        }
//        even number to odd
//        System.out.println(arr);
//        ListIterator itr=arr.listIterator();
//        while (itr.hasNext()){
//            int i =(int ) itr.next();
//            if(i%2==0){
//                i++;
//                itr.set(i);
//            }
//        }
//        System.out.println(arr);

    }
}
