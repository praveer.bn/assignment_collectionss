package week6.Assignment_collection;



import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;




 class Employee {
    int id;
    String name;
    float salary;

    public Employee(int id, String name, float salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", salary=" + salary +
                '}';
    }

}
class SortByName implements Comparator<Employee>{

    @Override
    public int compare(Employee o1, Employee o2) {
        return o1.name.compareTo(o2.name);
    }
}
public class EmployeeOperation{
    public static void main(String[] args) {
        List<Employee> arrayList = new ArrayList<>();
        arrayList.add(new Employee(1,"Ashraf",20000));
        arrayList.add(new Employee(2,"Aman",15000));
        arrayList.add(new Employee(3,"Praveer",30000));
        arrayList.add(new Employee(4,"Sanyami",10000));
        arrayList.add(new Employee(5,"Ayushman",13000));

        System.out.println("Before :: ");
        for(Employee employee : arrayList) {
            System.out.println(employee);
        }

        System.out.println("After sorting according to name : ");
        Collections.sort(arrayList,new SortByName());
        arrayList.stream().forEach(System.out::println);

        System.out.println("*************SALARY GREATER THAN 15000  ************");
        arrayList.stream().filter(p -> p.salary > 15000)
                .forEach(System.out::println);
    }
}
