package week6.Assignment_collection;


import java.util.ArrayDeque;
import java.util.Deque;

public class DequeDemo {
    public static void main(String[] args) {
        Deque<Integer> deque = new ArrayDeque<>();
        deque.add(1);
        deque.add(2);
        deque.add(3);
        deque.add(4);
        deque.add(5);

        System.out.println(deque);
//add first and last
        deque.addFirst(0);
        System.out.println(deque);

        deque.addLast(6);
        System.out.println(deque);

        System.out.println("element() : "+deque.element());
        System.out.println("getFirst() : "+deque.getFirst());
        System.out.println("getLast() : "+deque.getLast());
        System.out.println("peekFirst() : "+deque.peekFirst());
        System.out.println("peekLast() : "+deque.peekLast());

        System.out.println("peek() : "+deque.peek());
        deque.offer(1000);
        deque.offerFirst(11);
        deque.offerLast(15);

        System.out.println(deque);


    }
}